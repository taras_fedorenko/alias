﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import { CreateTeamsComponent } from "../create-teams/create-teams.component";
import { WelcomePageComponent } from "../welcome-page/welcome-page.component";
import { BaseService } from "../services/base.service";
import { GameService } from "../services/game.service";
import { WordService } from "../services/word.service";
import { TeamService } from "../services/team.service";
import { GameComponent } from "../game/game.component";
import { SignalRService } from 'src/services/signalR.service';

@NgModule({
  declarations: [
    AppComponent,
    WelcomePageComponent,
    CreateTeamsComponent,
    GameComponent
  ],
  imports: [
    BrowserModule, HttpClientModule, FormsModule, AppRoutingModule
  ],
  providers: [BaseService, GameService, WordService, TeamService, SignalRService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
