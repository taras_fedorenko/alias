﻿import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {WelcomePageComponent} from "../welcome-page/welcome-page.component";
import {CreateTeamsComponent} from "../create-teams/create-teams.component";
import {GameComponent} from "../game/game.component";


const routes: Routes = [
  { path: '', component: WelcomePageComponent },
  { path: 'create-teams', component: CreateTeamsComponent },
  { path: 'game', component: GameComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
