import { Injectable } from '@angular/core';
import * as signalR from '@microsoft/signalr';
import { BaseService } from './base.service';

@Injectable({
    providedIn: 'root'
})
export class SignalRService extends BaseService {
    private hubConnection: signalR.HubConnection;

    constructor() {
        super();
        this.hubConnection = new signalR.HubConnectionBuilder()
            .withUrl('api/gameHub', {
                skipNegotiation: true,
                transport: signalR.HttpTransportType.WebSockets
            })
            .withAutomaticReconnect()
            .build();

        this.hubConnection.onclose(error => {
            console.warn('Connection closed:', error);
        });
        this.hubConnection.onreconnecting(error => {
            console.log('Reconnecting:', error);
        });
        this.hubConnection.onreconnected(() => {
            console.log('Reconnected');
        });
        this.ensureConnection();
    }

    private startConnection(): void {
        this.hubConnection
            .start()
            .then(() => console.log('SignalR connection started.'))
            .catch(err => {
                console.error('Error while starting SignalR connection: ', err);
            });
    }

    private async ensureConnection(): Promise<void> {
        if (this.hubConnection.state === signalR.HubConnectionState.Connected) {
            return;
        }
        if (this.hubConnection.state === signalR.HubConnectionState.Disconnected) {
            try {
                this.startConnection();
            } catch (err) {
                console.error('Error while starting SignalR connection:', err);
                throw err;
            }
        }
        await this.waitForConnection();
        console.log('SignalR connection established');
    }
    
    private async waitForConnection(timeoutMs = 5000): Promise<void> {
        const startTime = Date.now();
    
        while (this.hubConnection.state !== signalR.HubConnectionState.Connected) {
            if (Date.now() - startTime > timeoutMs) {
                throw new Error('SignalR connection timed out.');
            }
            await new Promise(resolve => setTimeout(resolve, 2000));
        }
    }

    private async invokeSafe(methodName: string, ...args: any[]): Promise<any> {
        await this.ensureConnection();
        return this.hubConnection.invoke(methodName, ...args);
    }

    private async onSafe(eventName: string, callback: (...args: any[]) => void): Promise<any> {
        await this.ensureConnection();
        this.hubConnection.on(eventName, callback);
    }

    onTeamUpdated(callback: (teamId: string) => void): void {
        this.onSafe('TeamUpdated', callback);
    }
    
    onTeamScoreUpdated(callback: (teamId: string, score: number) => void): void {
        this.onSafe('TeamScoreUpdated', callback);
    }

    startGame(gameId: string): void {
        this.invokeSafe('StartGame', gameId)
            .catch(err => console.error('Error while starting game:', err));
    }

    onGameStart(callback: () => void): void {
        this.onSafe('StartGame', callback);
    }

    onCurrentPlayerChanged(callback: () => void): void {
        this.onSafe('CurrentPlayerChanged', callback);
    }

    onCurrentWordChanged(callback: (word: string) => void): void {
        this.onSafe('CurrentWordChanged', callback);
    }

    joinGameGroup(gameId: string): void {
        this.invokeSafe('JoinGameGroup', gameId)
            .catch(err => console.error('Error while joining group:', err));
    }

    leaveGameGroup(gameId: string): void {
        this.invokeSafe('LeaveGameGroup', gameId)
            .catch(err => console.error('Error while leaving group:', err));
    }
}
