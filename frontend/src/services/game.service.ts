import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GameInfo } from '../models/game-info.model';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root',
})
export class GameService extends BaseService {
  private readonly storageGameIdentifierKey = 'gameIdentifier';

  constructor(private http: HttpClient, private router: Router) {
      super();
  }

  addGame(): Observable<GameInfo> {
    return this.http.post<GameInfo>(`${this.apiUrl}/Game/addGame`, {});
  }

  updateGameIndexes(game: GameInfo): Observable<GameInfo> {
    return this.http.post<GameInfo>(`${this.apiUrl}/Game/updateGameIndexes`, game);
  }

  getGame(id: string): Observable<GameInfo> {
    return this.http.get<GameInfo>(`${this.apiUrl}/Game/getGame/${id}`);
  }

  setGameIdentifier(identifier: string): void {
    localStorage.setItem(this.storageGameIdentifierKey, identifier);
  }

  getGameIdentifier(): string {
    return localStorage.getItem(this.storageGameIdentifierKey) || '';
  }
}
