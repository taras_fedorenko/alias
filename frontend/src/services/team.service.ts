import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, Observable, tap, throwError } from 'rxjs';
import { TeamInfo } from '../models/team-info.model';
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root',
})
export class TeamService extends BaseService {

  constructor(private http: HttpClient) {
      super();
  }

  addTeam(team: TeamInfo): Observable<TeamInfo> {
    return this.http.post<TeamInfo>(`${this.apiUrl}/Team/addTeam`, team).pipe(
      catchError(error => {
        console.error('Error occurred while adding team:', error);
        return throwError(() => new Error('Error occurred while adding team'));
      })
    );
  }
  
  setScore(teamId: string, score: number): Observable<boolean> {
    return this.http.post<boolean>(`${this.apiUrl}/Team/setScore/${teamId}/${score}`, {}).pipe(
      catchError(error => {
        console.error('Error occurred while setting score:', error);
        return throwError(() => new Error('Error occurred while setting score'));
      })
    );
  }

  getTeam(id: string): Observable<TeamInfo> {
    return this.http.get<TeamInfo>(`${this.apiUrl}/Team/getTeam/${id}`).pipe(
      catchError(error => {
        console.error('Error occurred while getting team:', error);
        return throwError(() => new Error('Error occurred while getting team'));
      })
    );
  }

  deleteTeam(id: string): Observable<boolean> {
    return this.http.delete<boolean>(`${this.apiUrl}/Team/deleteTeam/${id}`).pipe(
      catchError(error => {
        console.error('Error occurred while deleting team:', error);
        return throwError(() => new Error('Error occurred while deleting team'));
      })
    );
  }
}
