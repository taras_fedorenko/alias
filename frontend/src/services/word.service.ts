import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { WordInfo } from '../models/word-info.model';
import { BaseService } from './base.service';


@Injectable({
  providedIn: 'root',
})
export class WordService extends BaseService {

  constructor(private http: HttpClient) {
      super();
  }

  getRandomWord(level: number, gameId: string): Observable<WordInfo> {
    return this.http.get<WordInfo>(`${this.apiUrl}/Word/getRandomWord/${gameId}/${level}`);
  }
}
