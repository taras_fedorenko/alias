export interface WordInfo {
  id: number;
  level: number;
  value: string;
}
