export enum UserRole {
  Guesser = 'guesser',
  Observer = 'observer',
  Explainer = 'explainer'
}
