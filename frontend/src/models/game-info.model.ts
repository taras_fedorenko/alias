import { TeamInfo } from "./team-info.model";

export interface GameInfo {
  id: string;
  currentTeamIndex: number;
  currentPlayerIndex: number;
  teams: TeamInfo[];
}
