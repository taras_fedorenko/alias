export interface TeamInfo {
  id: string;
  score: number;
  players: PlayerInfo[];
  name: string;
  gameId: string;
}

export interface PlayerInfo {
  id: string;
  name: string;
  teamId: string;
}
