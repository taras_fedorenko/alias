import { Component, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { lastValueFrom, Subscription } from 'rxjs';
import { PlayerInfo, TeamInfo } from '../models/team-info.model';
import { GameService } from '../services/game.service';
import { TeamService } from '../services/team.service';
import { SignalRService } from 'src/services/signalR.service';
import { GameInfo } from 'src/models/game-info.model';
import { UserRole } from 'src/models/user-role.model';

@Component({
  selector: 'app-create-teams',
  templateUrl: './create-teams.component.html',
  styleUrls: ['./create-teams.component.css']
})
export class CreateTeamsComponent {
  private playerName: string = '';
  private playerIdentifier: string = '';
  public gameId: string = '';
  private game: GameInfo = {} as GameInfo;
  private emptyId: string = '00000000-0000-0000-0000-000000000000';
  private subscriptions: Subscription[] = [];
  public teams: TeamInfo[] = [];
  public canStartGame: boolean = true;  
  public alertMessage: string = '';

  constructor(
    private router: Router,
    private gameService: GameService,
    private teamService: TeamService,
    private cdr: ChangeDetectorRef,
    private signalRService: SignalRService) { }

  createOrUpdateTeam(index: number): void {
    this.saveTeam(index);
  }

  copyToClipboard(): void {
    const message = 'Ідентифікатор гри скопійовано в буфер обміну';
    if (navigator && navigator.clipboard) {
      navigator.clipboard.writeText(this.gameId).then(() => {
        this.showAlert(message);
      }).catch(err => {
        console.error('Помилка при копіюванні в буфер обміну: ', err);
        this.fallbackCopyTextToClipboard();
        this.showAlert(message);
      });
    } else {
      this.fallbackCopyTextToClipboard();
      this.showAlert(message);
    }
  }
  
  private fallbackCopyTextToClipboard(): void {
    let selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = this.gameId;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    console.error('Виконана альтернативна копія в буфер обміну');
  }

  showAlert(message: string): void {
    this.alertMessage = message;
    setTimeout(() => this.alertMessage = '', 1000);
  }

  startGame(): void {
    if (this.teams.length > 1) {
      const incompleteTeams = this.teams.filter(team => team.players.length === 1);
      if (incompleteTeams.length > 0) {
        incompleteTeams.forEach(async team => {
          const player = team.players[0];
          const incompleteTeamIndex = this.teams.indexOf(team);
          await this.removeTeam(incompleteTeamIndex);
          const randomTeamIndex = Math.floor(Math.random() * this.teams.length);
          const teamId = this.teams[randomTeamIndex].id;
          player.teamId = teamId;
          this.teams[randomTeamIndex].players.push(player);
          this.createOrUpdateTeam(randomTeamIndex);
        });
        this.cdr.detectChanges();
      }
      if (this.teams.length > 1) {
        this.signalRService.startGame(this.gameId);
      } else {
        console.error('Не достатньо команд для гри');
      }
    }
  }

  async removeTeam(index: number): Promise<void> {
    const teamId = this.teams[index].id;
    if (teamId && teamId !== this.emptyId) {
      await this.deleteTeam(teamId, index);
    }
  }

  trackByIndex(index: number, item: any): number {
    return index;
  }

  ngOnInit() {
    this.signalRService.onTeamUpdated(() => {
      this.reloadComponent();
    });
    this.signalRService.onGameStart(() => {
      this.router.navigate(['/game']);
    });
    this.playerName = localStorage.getItem('playerName') || '';
    this.playerIdentifier = localStorage.getItem('playerIdentifier') || '';
    this.gameId = this.gameService.getGameIdentifier();
    if (this.gameId === '') {
      this.addGame();      
      localStorage.setItem('isAdmin', 'true');
    } else {
      this.getTeamsByGameId(this.gameId);
    }
    this.isStartGameAllowed(); 
  }

  isStartGameAllowed(): void {
    const isAdmin = localStorage.getItem('isAdmin') === 'true';
    this.canStartGame = isAdmin;
  }
 
  getTeamsByGameId(id: string) {    
    this.signalRService.joinGameGroup(this.gameId);
    const subscription = this.gameService.getGame(id).subscribe({
      next: (result) => {
        this.game = result;
        this.teams = result.teams;
        this.manageTeams();
      },
      error: (error) => {
        console.error(error);
      }
    });

    this.subscriptions.push(subscription);
  }

  async deleteTeam(teamId: string, index: number): Promise<void> {
    try {
      const result = await lastValueFrom(this.teamService.deleteTeam(teamId));
      if (this.teams.length > 1) {
        this.teams.splice(index, 1);
      }
    } catch (error) {
      console.error('Error deleting team:', error);
    }
  }


  saveTeam(index: number) {
    const team = this.teams[index];
    team.gameId = this.gameId;
    const subscription = this.teamService.addTeam(team).subscribe({
      next: (result) => {
        this.teams[index] = result;
        this.setRoles();
      },
      error: (error) => {
        console.error(error);
      }
    });

    this.subscriptions.push(subscription);
  }

  private setRoles(): void {
    const currentTeam = this.teams[this.game.currentTeamIndex];
    const currentPlayer = currentTeam.players[this.game.currentPlayerIndex];
  
    if (currentPlayer.id === this.playerIdentifier) {
      localStorage.setItem('userRole', UserRole.Explainer);
    } else if (currentTeam.players.some(player => player.id === this.playerIdentifier)) {
      localStorage.setItem('userRole', UserRole.Guesser);
    } else {
      localStorage.setItem('userRole', UserRole.Observer);
    }
  }
 
  addGame() {
    const gameIdSubscription = this.gameService.addGame().subscribe({
      next: (result) => {
        this.game = result;
        this.gameId = result.id;
        this.gameService.setGameIdentifier(this.gameId)
        this.signalRService.joinGameGroup(this.gameId);
        this.manageTeams();
      },
      error: (error) => {
        console.error(error);
      }
    });

    this.subscriptions.push(gameIdSubscription);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  private reloadComponent(): void {
    this.ngOnInit();
  }

  private generateRandomTeamName(): string {
    const adjectives = [
      'Mighty', 'Brave', 'Swift', 'Fierce', 'Valiant', 'Noble', 'Bold', 'Fearless', 'Gallant', 'Heroic',
      'Intrepid', 'Loyal', 'Majestic', 'Powerful', 'Resolute', 'Steadfast', 'Strong', 'Vigilant', 'Wise', 'Courageous'
    ];
    const animals = [
      'Lions', 'Tigers', 'Bears', 'Wolves', 'Eagles', 'Hawks', 'Panthers', 'Leopards', 'Cheetahs', 'Falcons',
      'Sharks', 'Dragons', 'Griffins', 'Phoenixes', 'Cobras', 'Ravens', 'Stallions', 'Buffaloes', 'Rhinos', 'Elephants'
    ];
    const adjective = adjectives[Math.floor(Math.random() * adjectives.length)];
    const animal = animals[Math.floor(Math.random() * animals.length)];
    const randomNumber = Math.floor(Math.random() * 100) + 1;
    return `${adjective} ${animal} ${randomNumber}`;
  }

  private manageTeams(): void {
    if (!this.playerName || !this.playerIdentifier) {
      console.error('Player data is missing in localStorage');
      return;
    }
    for (const team of this.teams) {
      if (team.players.some(player => player.id === this.playerIdentifier)) {
        console.log('Player is already in a team');
        return;
      }
    }
    let teamIndex: number = 0;
    if (this.teams.length === 0 || this.teams.every(team => team.players.length > 1)) {
      const newTeam = {
        name: this.generateRandomTeamName(),
        players: [{ name: this.playerName, id: this.playerIdentifier }]
      } as TeamInfo;
      teamIndex = this.teams.length;
      this.teams.push(newTeam);
    } else {
      const availableTeam = this.teams.find(team => team.players.length < 2);
      if (availableTeam) {
        availableTeam.players.push({
          name: this.playerName,
          id: this.playerIdentifier,
          teamId: availableTeam.id
        } as PlayerInfo);
        teamIndex = this.teams.indexOf(availableTeam);
      }
    }
    this.createOrUpdateTeam(teamIndex);
  }
}
