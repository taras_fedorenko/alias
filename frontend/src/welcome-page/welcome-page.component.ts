import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GameService } from '../services/game.service';

@Component({
  selector: 'app-welcome-page',
  templateUrl: './welcome-page.component.html',
  styleUrls: ['./welcome-page.component.css']
})
export class WelcomePageComponent implements OnInit {
  playerName: string = '';
  gameIdentifier: string = '';  
  playerIdentifier: string = '';
  playerNameInvalid: boolean = false;
  gameIdentifierInvalid: boolean = false;
  constructor(private router: Router, private gameService: GameService) { }

  ngOnInit(): void {
  }

  validatePlayerName() {
    const pattern = /^[a-zA-Zа-яА-ЯіїєґІЇЄҐ0-9]+$/;
    this.playerNameInvalid = !pattern.test(this.playerName);
  }

  validateGameIdentifier() {
    const guidPattern = /^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$/;
    this.gameIdentifierInvalid = !guidPattern.test(this.gameIdentifier);
  }

  trimSpaces() {
    this.gameIdentifier = this.gameIdentifier.replace(/\s+/g, '');
    this.validateGameIdentifier();
  }

  isFormValid() {
    return this.playerName && !this.playerNameInvalid;
  }

  startGame(): void {
    this.savePlayerData();
    this.gameService.setGameIdentifier(this.gameIdentifier);
    this.router.navigate(['/create-teams']);
  }

  private savePlayerData(): void {
    localStorage.setItem('playerName', this.playerName);
    this.playerIdentifier = this.generateGUID();
    localStorage.setItem('playerIdentifier', this.playerIdentifier);
  }

  private generateGUID(): string {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      const r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }
}
