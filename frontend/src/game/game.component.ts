import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { GameInfo } from '../models/game-info.model';
import { TeamInfo } from '../models/team-info.model';
import { GameService } from '../services/game.service';
import { TeamService } from '../services/team.service';
import { WordService } from '../services/word.service';
import { UserRole } from 'src/models/user-role.model';
import { SignalRService } from 'src/services/signalR.service';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription[] = [];
  timerRunning: boolean = false;
  public gameId: string = '';
  public teams: TeamInfo[] = [];
  currentTeamIndex: number = 0;
  currentPlayerIndex: number = 0;
  timerDefaultValue: number = 60;
  timer: number = this.timerDefaultValue;
  timerInterval: any;
  currentWordValue: string = '';
  hasWordPermission: boolean = false;
  playerIdentifier: string = '';
  playerName: string = '';

  constructor(private wordService: WordService,
    private teamService: TeamService,
    private gameService: GameService,
    private signalRService: SignalRService
  ) { }

  ngOnInit(): void {
    this.gameId = this.gameService.getGameIdentifier();
    this.playerIdentifier = localStorage.getItem('playerIdentifier') || '';
    this.playerName = localStorage.getItem('playerName') || '';
    this.signalRService.onCurrentPlayerChanged(() => {
      console.log('onCurrentPlayerChanged');
      this.setRoles();
      this.checkWordPermission();
      this.ngOnInit();
    });
    this.signalRService.onCurrentWordChanged((word: string) => {
      console.log('onCurrentWordChanged:', word);
      this.currentWordValue = word;
    });
    this.signalRService.onTeamScoreUpdated((teamId: string, score: number) => {
      console.log('onTeamScoreUpdated:', teamId);
      this.teams[this.currentTeamIndex].score = score;
    });
    this.initGame(this.gameId);
  }

  ngOnDestroy(): void {
    clearInterval(this.timerInterval);
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  startGame(): void {
    this.timerRunning = true;
    this.timerInterval = setInterval(() => {
      if (this.timer > 0) {
        this.timer--;
      } else {
        this.setScore();
        clearInterval(this.timerInterval);
        this.timerRunning = false;
        this.nextPlayer();
        this.updateGameIndexes();
        this.ngOnInit();
      }
    }, 1000);

    this.showNextWord();
  }

  nextPlayer(): void {
    this.moveToNextPlayerOrTeam();
    this.timer = this.timerDefaultValue;
  }

  skipWord(): void {
    this.teams[this.currentTeamIndex].score--;
    this.showNextWord();
  }

  correctGuess(): void {
    this.teams[this.currentTeamIndex].score++;
    this.showNextWord();
  }

  getMyTeamName(): string {
    for (let team of this.teams) {
      if (team.players.some(player => player.id === this.playerIdentifier)) {
        return team.name;
      }
    }
    return 'Команда не знайдена';
  }

  isAdmin(): boolean {
    return localStorage.getItem('isAdmin') === 'true';
  }

  increaseScore(team: TeamInfo) {
    team.score++;
    this.setScoreByTeamId(team.id, team.score);
  }

  decreaseScore(team: TeamInfo) {
    if (team.score > 0) {
      team.score--;
      this.setScoreByTeamId(team.id, team.score);
    }
  }

  private setScore(): void {
    const team = this.teams[this.currentTeamIndex];
    this.setScoreByTeamId(team.id, team.score);
  }

  private setScoreByTeamId(teamId: string, teamScore: number): void {
    this.teamService.setScore(teamId, teamScore || 0).subscribe({
      next: () => { },
      error: (error) => {
        console.error(error);
      }
    });
  }

  private checkWordPermission(): void {
    if (localStorage.getItem('userRole') === UserRole.Guesser) {
      this.hasWordPermission = false;
    } else {
      this.hasWordPermission = true;
    }
  }

  private setRoles(): void {
    const currentTeam = this.teams[this.currentTeamIndex];
    const currentPlayer = currentTeam.players[this.currentPlayerIndex];

    if (currentPlayer.id === this.playerIdentifier) {
      localStorage.setItem('userRole', UserRole.Explainer);
    } else if (currentTeam.players.some(player => player.id === this.playerIdentifier)) {
      localStorage.setItem('userRole', UserRole.Guesser);
    } else {
      localStorage.setItem('userRole', UserRole.Observer);
    }
  }

  initGame(id: string) {
    const subscription = this.gameService.getGame(id).subscribe({
      next: (result) => {
        this.teams = result.teams;
        this.currentTeamIndex = result.currentTeamIndex;
        this.currentPlayerIndex = result.currentPlayerIndex;
        this.setRoles();
        this.checkWordPermission();
      },
      error: (error) => {
        console.error(error);
      }
    });

    this.subscriptions.push(subscription);
  }

  private showNextWord(): void {
    const getRandomWordSubscription = this.wordService.getRandomWord(0, this.gameId).subscribe({
      next: (result) => {
        this.currentWordValue = result.value;
      },
      error: (error) => {
        console.error(error);
      }
    });

    this.subscriptions.push(getRandomWordSubscription);
  }

  nextPairOfIndices(index1: number, index2: number, numberOfRows: number, numberOfColumns: number): [number, number] {
    const maxRowIndex = numberOfRows - 1;
    const maxColumnIndex = numberOfColumns - 1;

    if (index1 < maxRowIndex) {
      return [index1 + 1, index2];
    } else if (index2 < maxColumnIndex) {
      return [0, index2 + 1];
    } else {
      return [0, 0];
    }
  }

  moveToNextPlayerOrTeam(): void {
    [this.currentTeamIndex, this.currentPlayerIndex] =
      this.nextPairOfIndices(this.currentTeamIndex,
        this.currentPlayerIndex,
        this.teams.length,
        this.teams[this.currentTeamIndex].players.length);
  }

  updateGameIndexes(): void {
    const gameInfo = {
      id: this.gameId,
      currentPlayerIndex: this.currentPlayerIndex,
      currentTeamIndex: this.currentTeamIndex,
      teams: this.teams
    } as GameInfo
    const subscription = this.gameService.updateGameIndexes(gameInfo).subscribe({
      next: (result) => {
        this.teams = result.teams;
        this.currentTeamIndex = result.currentTeamIndex;
        this.currentPlayerIndex = result.currentPlayerIndex;
        this.setRoles();
        this.checkWordPermission();
      },
      error: (error) => {
        console.error(error);
      }
    });

    this.subscriptions.push(subscription);
  }
}
