﻿namespace AliasApp.Server.Models;

/// <summary>
/// Represents the information of a game.
/// </summary>
public record GameInfo
{
    /// <summary>
    /// Gets the unique identifier for the game.
    /// </summary>
    public Guid Id { get; init; }

    /// <summary>
    /// Gets the index of the current team.
    /// </summary>
    public int CurrentTeamIndex { get; init; }

    /// <summary>
    /// Gets the index of the current player.
    /// </summary>
    public int CurrentPlayerIndex { get; init; }

	/// <summary>
	/// Gets the list of teams in the game.
	/// </summary>
	public List<TeamInfo> Teams { get; init; } = [];
}
