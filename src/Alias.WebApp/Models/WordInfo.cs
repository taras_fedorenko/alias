namespace AliasApp.Server.Models;

using AliasApp.Server.Enums;

#nullable disable

/// <summary>
/// Represents the information of a word.
/// </summary>
public record WordInfo
{
    /// <summary>
    /// Gets the unique identifier for the word.
    /// </summary>
    public int Id { get; init; }

    /// <summary>
    /// Gets the difficulty level of the word.
    /// </summary>
    public Level Level { get; init; }

    /// <summary>
    /// Gets the value of the word.
    /// </summary>
    public string Value { get; init; }
}
