namespace AliasApp.Server.Models;

/// <summary>
/// Represents the information of a team.
/// </summary>
public record TeamInfo
{
    /// <summary>
    /// Gets the unique identifier for the team.
    /// </summary>
    public Guid Id { get; set; }

    /// <summary>
    /// Gets or sets the score of the team.
    /// </summary>
    public int? Score { get; init; }

    /// <summary>
    /// Gets the unique identifier of the game to which the team belongs.
    /// </summary>
    public Guid GameId { get; init; }

    /// <summary>
    /// Gets the list of players in the team.
    /// </summary>
    public List<PlayerInfo> Players { get; init; } = [];

    /// <summary>
    /// Gets or sets the name of the team.
    /// </summary>
    public required string Name { get; init; }
}

/// <summary>
/// Represents the information of a player.
/// </summary>
public record PlayerInfo
{
    /// <summary>
    /// Gets the unique identifier for the player.
    /// </summary>
    public Guid Id { get; init; }

	/// <summary>
	/// Gets or sets the name of the player.
	/// </summary>
	public required string Name { get; init; }

    /// <summary>
    /// Gets the unique identifier of the team to which the player belongs.
    /// </summary>
    public Guid TeamId { get; init; }
}
