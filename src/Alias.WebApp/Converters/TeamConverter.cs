﻿namespace AliasApp.Server.Converters;

using AliasApp.Server.Models;

/// <summary>
/// Provides methods to convert between Team and TeamInfo models.
/// </summary>
public static class TeamConverter
{
    /// <summary>
    /// Converts a Team entity to a TeamInfo model.
    /// </summary>
    /// <param name="team">The Team entity to convert.</param>
    /// <returns>The converted TeamInfo model.</returns>
    public static TeamInfo ConvertToTeamModel(Team team) => new()
    {
        Id = team.Id,
        Score = team.Score,
        GameId = team.GameId,
        Players = ConvertToPlayerModels(team.Players),
        Name = team.Name
    };

    /// <summary>
    /// Converts a TeamInfo model to a Team entity.
    /// </summary>
    /// <param name="teamModel">The TeamInfo model to convert.</param>
    /// <returns>The converted Team entity.</returns>
    public static Team ConvertToTeam(TeamInfo teamModel) => new()
    {
        Id = teamModel.Id,
        GameId = teamModel.GameId,
        Score = teamModel.Score,
        Players = ConvertToPlayers(teamModel.Players),
        Name = teamModel.Name
    };

    /// <summary>
    /// Converts a list of PlayerInfo models to a list of Player entities.
    /// </summary>
    /// <param name="playerModels">The list of PlayerInfo models to convert.</param>
    /// <returns>The converted list of Player entities.</returns>
    private static List<Player> ConvertToPlayers(List<PlayerInfo> playerModels) =>
        playerModels.Select(ConvertToPlayer).ToList();

	/// <summary>
	/// Converts a PlayerInfo model to a Player entity.
	/// </summary>
	/// <param name="playerModel">The PlayerInfo model to convert.</param>
	/// <returns>The converted Player entity.</returns>
	private static Player ConvertToPlayer(PlayerInfo playerModel) => new()
    {
        Id = playerModel.Id,
        Name = playerModel.Name,
        TeamId = playerModel.TeamId
    };

    /// <summary>
    /// Converts a list of Player entities to a list of PlayerInfo models.
    /// </summary>
    /// <param name="players">The list of Player entities to convert.</param>
    /// <returns>The converted list of PlayerInfo models.</returns>
    private static List<PlayerInfo> ConvertToPlayerModels(List<Player> players) =>
        players.Select(ConvertToPlayerModel).ToList();

    /// <summary>
    /// Converts a Player entity to a PlayerInfo model.
    /// </summary>
    /// <param name="player">The Player entity to convert.</param>
    /// <returns>The converted PlayerInfo model.</returns>
    private static PlayerInfo ConvertToPlayerModel(Player player) => new()
    {
        Id = player.Id,
        Name = player.Name,
        TeamId = player.TeamId,
    };
}
