﻿namespace AliasApp.Server.Converters;

using AliasApp.Server.Models;

public static class GameConverter
{
    /// <summary>
    /// Converts a <see cref="Game"/> object to a <see cref="GameInfo"/> object.
    /// </summary>
    /// <param name="game">The <see cref="Game"/> object to convert.</param>
    /// <returns>The converted <see cref="GameInfo"/> object.</returns>
    public static GameInfo ConvertToGameModel(Game game) => new()
    {
        Id = game.Id,
        CurrentTeamIndex = game.CurrentTeamIndex,
        CurrentPlayerIndex = game.CurrentPlayerIndex,
        Teams = game.Teams.Select(TeamConverter.ConvertToTeamModel).ToList()
    };
}
