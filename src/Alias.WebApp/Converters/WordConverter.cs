﻿namespace AliasApp.Server.Converters;

using AliasApp.Server.Models;

public static class WordConverter
{
    /// <summary>
    /// Converts a Word object to a WordInfo object.
    /// </summary>
    /// <param name="word">The Word object to convert.</param>
    /// <returns>The converted WordInfo object.</returns>
    public static WordInfo ConvertToWordModel(Word word) => new()
    {
        Id = word.Id,
        Level = word.Level,
        Value = word.Value
    };
}
