﻿namespace AliasApp.Server.Enums;

/// <summary>
/// Represents the difficulty levels.
/// </summary>
public enum Level
{
    /// <summary>
    /// Easy level.
    /// </summary>
    Easy = 0,

    /// <summary>
    /// Standard level.
    /// </summary>
    Standard = 1,

    /// <summary>
    /// Hard level.
    /// </summary>
    Hard = 2
}
