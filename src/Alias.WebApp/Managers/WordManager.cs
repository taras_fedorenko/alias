﻿namespace AliasApp.Server.Managers;

using AliasApp.Server.Converters;
using AliasApp.Server.Enums;
using AliasApp.Server.Models;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;

public class WordManager : IWordManager
{
    private readonly AppDbContext _dbContext;
    private readonly IMemoryCache _memoryCache;
	private readonly IHubContext<GameHub> _hubContext;

	/// <summary>
	/// Initializes a new instance of the <see cref="WordManager"/> class.
	/// </summary>
	/// <param name="dbContext">The application database context.</param>
	/// <param name="memoryCache">The memory cache.</param>
	/// <param name="hubContext">The hub context.</param>
	public WordManager(AppDbContext dbContext, IMemoryCache memoryCache, IHubContext<GameHub> hubContext)
    {
        _dbContext = dbContext;
        _memoryCache = memoryCache;
		_hubContext = hubContext;
	}

    #region Public Methods

    /// <summary>
    /// Gets the words for the specified level.
    /// </summary>
    /// <param name="level">The difficulty level.</param>
    /// <returns>The list of word information.</returns>
    public async Task<IEnumerable<WordInfo>> GetWordsAsync(Level level)
    {
        var words = await _dbContext.Words.Where(x => x.Level == level).ToListAsync();
        var wordModels = words.Select(WordConverter.ConvertToWordModel).ToList();
        return wordModels;
    }

    /// <summary>
    /// Gets the word with the specified value.
    /// </summary>
    /// <param name="wordValue">The value of the word.</param>
    /// <returns>The word information.</returns>
    public async Task<WordInfo> GetWordAsync(string wordValue)
    {
        var word = await _dbContext.Words.FirstOrDefaultAsync(x => x.Value == wordValue);
        if (word == null)
        {
            return new WordInfo();
        }

        var wordModel = WordConverter.ConvertToWordModel(word);
        return wordModel;
    }

    /// <summary>
    /// Deletes the word with the specified ID.
    /// </summary>
    /// <param name="wordId">The ID of the word.</param>
    /// <returns>A boolean indicating whether the word was deleted successfully.</returns>
    public async Task<bool> DeleteWordAsync(int wordId)
    {
        var word = await _dbContext.Words.FindAsync(wordId);
        if (word == null)
        {
            return true;
        }

        _dbContext.Words.Remove(word);
        await _dbContext.SaveChangesAsync();

        return true;
    }

    /// <summary>
    /// Adds a new word with the specified level and value.
    /// </summary>
    /// <param name="level">The difficulty level of the word.</param>
    /// <param name="value">The value of the word.</param>
    /// <returns>A boolean indicating whether the word was added successfully.</returns>
    public async Task<bool> AddWordAsync(Level level, string value)
    {
        var word = new Word
        {
            Level = level,
            Value = value
        };

        _dbContext.Words.Add(word);
        await _dbContext.SaveChangesAsync();

        return true;
    }

	/// <summary>
	/// Gets a random word for the specified level.
	/// </summary>
	/// <param name="level">The difficulty level.</param>
	/// <param name="gameId">The game identifier.</param>
	/// <returns>The random word information.</returns>
	public async Task<WordInfo> GetRandomWordAsync(Level level, Guid gameId)
    {
        var randomWord = await GetUniqueRandomWordAsync(level) ?? throw new InvalidOperationException("No words to see");
        var wordModel = new WordInfo
        {
            Id = randomWord.Id,
            Level = randomWord.Level,
            Value = randomWord.Value
        };

		await NotifyCurrentWordChangedAsync(gameId, randomWord.Value);

		return wordModel;
    }

    #endregion

    #region Private Methods

    private async Task<WordInfo> GetUniqueRandomWordAsync(Level level)
    {
        var dictKey = $"WordDictionary_Level_{level}";
        if (!_memoryCache.TryGetValue(dictKey, out Dictionary<int, WordInfo>? wordDictionary) || wordDictionary?.Count == 0)
        {
            var allWords = await GetWordsAsync(level);
            wordDictionary = allWords.ToDictionary(w => w.Id, w => w);
            _memoryCache.Set(dictKey, wordDictionary, new MemoryCacheEntryOptions
            {
                AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(360)
            });
        }
        WordInfo? randomWord = null;
        var uniqueId = GetUniqueRandomId(wordDictionary?.Keys);
        wordDictionary?.TryGetValue(uniqueId, out randomWord);
        wordDictionary?.Remove(uniqueId);

        return randomWord ?? throw new InvalidOperationException("The word is empty."); ;
    }

    private static int GetUniqueRandomId(ICollection<int>? ids)
    {
        if (ids == null || ids.Count == 0)
        {
            throw new InvalidOperationException("The dictionary is empty.");
        }

        var random = new Random();
        var randomIndex = random.Next(0, ids.Count);

        return ids.ElementAt(randomIndex);
    }

	/// <summary>
	/// Notifies the clients in a game group that a team has been updated.
	/// </summary>
	/// <param name="gameId">The ID of the game.</param>
	/// <param name="teamId">The ID of the team.</param>
	/// <returns>A task representing the asynchronous operation.</returns>
	private async Task NotifyCurrentWordChangedAsync(Guid gameId, string word)
	{
		await _hubContext.Clients.Group(gameId.ToString()).SendAsync("CurrentWordChanged", word);
	}

	#endregion
}
