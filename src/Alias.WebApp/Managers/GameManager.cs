﻿namespace AliasApp.Server.Managers;

using AliasApp.Server.Converters;
using AliasApp.Server.Models;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;

public class GameManager : IGameManager
{
    private readonly AppDbContext _dbContext;
	private readonly IHubContext<GameHub> _hubContext;

	public GameManager(AppDbContext dbContext, IHubContext<GameHub> hubContext)
    {
		_hubContext = hubContext;
        _dbContext = dbContext;
    }

	#region Methods: Private

	/// <summary>
	/// Notifies the clients in a game group that a current player has been changed.
	/// </summary>
	/// <param name="gameId">The ID of the game.</param>
	/// <returns>A task representing the asynchronous operation.</returns>
	private async Task NotifyCurrentPlayerChangedAsync(Guid gameId)
	{
		await _hubContext.Clients.Group(gameId.ToString()).SendAsync("CurrentPlayerChanged");
	}

	#endregion

	#region Methods: Public

	/// <summary>
	/// Retrieves a game by its unique identifier.
	/// </summary>
	/// <param name="gameId">The unique identifier of the game.</param>
	/// <returns>The game information.</returns>
	public async Task<GameInfo> GetGameAsync(Guid gameId)
    {
        var game = await _dbContext.Games
            .Include(x => x.Teams)
            .ThenInclude(x => x.Players)
            .FirstAsync(x => x.Id == gameId);

        var gameInfo = GameConverter.ConvertToGameModel(game);

        return gameInfo;
    }

    /// <summary>
    /// Retrieves all games.
    /// </summary>
    /// <returns>The list of game information.</returns>
    public async Task<IEnumerable<GameInfo>> GetGamesAsync()
    {
        var games = await _dbContext.Games
            .Include(x => x.Teams)
            .ThenInclude(x => x.Players)
            .ToListAsync();
        var gamesInfo = games.Select(GameConverter.ConvertToGameModel).ToList();
        return gamesInfo;
    }

    /// <summary>
    /// Deletes a game by its unique identifier.
    /// </summary>
    /// <param name="gameId">The unique identifier of the game.</param>
    /// <returns>True if the game was deleted successfully, otherwise false.</returns>
    public async Task<bool> DeleteGameAsync(Guid gameId)
    {
        var game = await _dbContext.Games.FindAsync(gameId);

        if (game == null)
        {
            return true;
        }

        _dbContext.Games.Remove(game);

        await _dbContext.SaveChangesAsync();

        return true;
    }

    /// <summary>
    /// Adds a new game.
    /// </summary>
    /// <param name="gameId">The optional unique identifier for the game. If not provided, a new unique identifier will be generated.</param>
    /// <returns>The unique identifier of the added game.</returns>
    public async Task<GameInfo> AddGameAsync(Guid? gameId = null)
    {
        var id = gameId ?? Guid.NewGuid();
        _dbContext.Games.Add(new Game { Id = id, CurrentPlayerIndex = 0, CurrentTeamIndex = 0 });
		
		await NotifyCurrentPlayerChangedAsync(id);
		
		await _dbContext.SaveChangesAsync();

        return await GetGameAsync(id);
    }

    /// <summary>
    /// Updates the game indexes.
    /// </summary>
    /// <param name="gameInfo">The game information containing the updated indexes.</param>
    /// <returns>The updated game information.</returns>
    public async Task<GameInfo> UpdateGameIndexesAsync(GameInfo gameInfo)
    {
        var game = await _dbContext.Games.FirstAsync(x => x.Id == gameInfo.Id);

        game.CurrentTeamIndex = gameInfo.CurrentTeamIndex;
        game.CurrentPlayerIndex = gameInfo.CurrentPlayerIndex;

        await _dbContext.SaveChangesAsync();

		await NotifyCurrentPlayerChangedAsync(gameInfo.Id);

        return await GetGameAsync(gameInfo.Id);
    }

    /// <summary>
    /// Notifies the clients in a game group that the game has started.
    /// </summary>
    /// <param name="gameId">The ID of the game.</param>
    /// <returns>A task representing the asynchronous operation.</returns>
    public async Task NotifyStartGameAsync(Guid gameId)
    {
        await _hubContext.Clients.Group(gameId.ToString()).SendAsync("StartGame");
    }

	#endregion
}
