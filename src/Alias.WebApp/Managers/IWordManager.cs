﻿namespace AliasApp.Server.Managers;

using AliasApp.Server.Enums;
using AliasApp.Server.Models;

public interface IWordManager
{
    Task<bool> AddWordAsync(Level level, string value);
    Task<bool> DeleteWordAsync(int wordId);
    Task<WordInfo> GetRandomWordAsync(Level level, Guid gameId);
    Task<WordInfo> GetWordAsync(string wordValue);
    Task<IEnumerable<WordInfo>> GetWordsAsync(Level level);
}
