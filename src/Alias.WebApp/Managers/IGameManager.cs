﻿namespace AliasApp.Server.Managers;

using AliasApp.Server.Models;

public interface IGameManager
{
    Task<GameInfo> AddGameAsync(Guid? gameId = null);
    Task<bool> DeleteGameAsync(Guid gameId);
    Task<GameInfo> GetGameAsync(Guid gameId);
    Task<IEnumerable<GameInfo>> GetGamesAsync();
    Task<GameInfo> UpdateGameIndexesAsync(GameInfo gameInfo);
	Task NotifyStartGameAsync(Guid gameId);
}
