﻿namespace AliasApp.Server.Managers;

using AliasApp.Server.Converters;
using AliasApp.Server.Models;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;

public class TeamManager : ITeamManager
{
    private readonly AppDbContext _dbContext;
	private readonly IHubContext<GameHub> _hubContext;

	public TeamManager(AppDbContext dbContext, IHubContext<GameHub> hubContext)
	{
		_hubContext = hubContext;
		_dbContext = dbContext;
    }

	#region Methods: Private
	/// <summary>
	/// Saves or updates a player.
	/// </summary>
	/// <param name="player">The player to save or update.</param>
	/// <returns>A task representing the asynchronous operation.</returns>
	private async Task AddOrUpdatePlayer(Player player)
    {
        var existingPlayer = await _dbContext.Players.FindAsync(player.Id);
        if (existingPlayer == null)
        {
            _dbContext.Players.Add(player);
        }
        else
        {
            existingPlayer.Name = player.Name;
            existingPlayer.TeamId = player.TeamId;
        }
        await _dbContext.SaveChangesAsync();
    }

    /// <summary>
    /// Notifies the clients in a game group that a team has been updated.
    /// </summary>
    /// <param name="gameId">The ID of the game.</param>
    /// <param name="teamId">The ID of the team.</param>
    /// <returns>A task representing the asynchronous operation.</returns>
    private async Task NotifyTeamUpdatedAsync(Guid gameId, Guid teamId)
    {
        await _hubContext.Clients.Group(gameId.ToString()).SendAsync("TeamUpdated", teamId);
    }

	/// <summary>
	/// Notifies the clients in a game group that a team has been updated.
	/// </summary>
	/// <param name="gameId">The ID of the game.</param>
	/// <param name="teamId">The ID of the team.</param>
	/// <returns>A task representing the asynchronous operation.</returns>
	private async Task NotifyTeamScoreUpdatedAsync(Guid gameId, Guid teamId, int score)
	{
		await _hubContext.Clients.Group(gameId.ToString()).SendAsync("TeamScoreUpdated", teamId, score);
	}

	#endregion

	#region Methods: Public 

	/// <summary>
	/// Gets all the teams.
	/// </summary>
	/// <returns>The list of team information.</returns>
	public async Task<IEnumerable<TeamInfo>> GetTeamsAsync()
    {
        var teams = await _dbContext.Teams
            .Include(x => x.Players)
            .ToListAsync();

        var teamModels = teams.Select(TeamConverter.ConvertToTeamModel).ToList();
        return teamModels;
    }

    /// <summary>
    /// Gets the team with the specified ID.
    /// </summary>
    /// <param name="teamId">The ID of the team.</param>
    /// <returns>The team information.</returns>
    public async Task<TeamInfo> GetTeamAsync(Guid teamId)
    {
        var team = await _dbContext.Teams
            .Include(x => x.Players)
            .FirstAsync(x => x.Id == teamId);

        var teamModel = TeamConverter.ConvertToTeamModel(team);

        return teamModel;
    }

    /// <summary>
    /// Deletes the teams.
    /// </summary>
    /// <param name="teamId">The ID of the team to delete. If null, deletes all teams.</param>
    /// <returns>A boolean indicating whether the teams were deleted successfully.</returns>
    public async Task<bool> DeleteTeamsAsync(Guid? teamId)
    {
        if (teamId == null)
        {
            var players = await _dbContext.Teams.ToListAsync();
            _dbContext.Teams.RemoveRange(players);
            var teams = await _dbContext.Teams.ToListAsync();
            _dbContext.Teams.RemoveRange(teams);
        }
        else
        {
            var players = await _dbContext.Players.Where(x => x.TeamId == teamId).ToListAsync();
            _dbContext.Players.RemoveRange(players);

            var team = await _dbContext.Teams.FindAsync(teamId);
            if (team == null)
            {
                return true;
            }
            _dbContext.Teams.Remove(team);
			await NotifyTeamUpdatedAsync(team.GameId, teamId ?? Guid.Empty);
		}

        await _dbContext.SaveChangesAsync();

        return true;
    }

    /// <summary>
    /// Adds or updates a team.
    /// </summary>
    /// <param name="teamInfo">The team information.</param>
    /// <returns>The added or updated team information.</returns>
    public async Task<TeamInfo> AddOrUpdateTeamAsync(TeamInfo teamInfo)
    {
        var team = TeamConverter.ConvertToTeam(teamInfo);
		var existingTeam = await _dbContext.Teams.FindAsync(team.Id);
		if (existingTeam == null)
        {
            _dbContext.Teams.Add(team);
        }
        else
        {
			foreach (var player in team.Players)
			{
				await AddOrUpdatePlayer(player);
			}
            existingTeam.Name = team.Name;
            existingTeam.Score = team.Score;
            existingTeam.GameId = team.GameId;
        }

        await _dbContext.SaveChangesAsync();
		
		await NotifyTeamUpdatedAsync(team.GameId, team.Id);

		return await GetTeamAsync(team.Id);
    }
	

	/// <summary>
	/// Sets the score for a team.
	/// </summary>
	/// <param name="teamId">The ID of the team.</param>
	/// <param name="score">The score to set.</param>
	/// <returns>A boolean indicating whether the score was set successfully.</returns>
	public async Task<bool> SetTeamScoreAsync(Guid teamId, int score)
    {
        var team = await _dbContext.Teams.FindAsync(teamId)
            ?? throw new ArgumentNullException("Team not found");
        team.Score = score;
        await _dbContext.SaveChangesAsync();
		await NotifyTeamScoreUpdatedAsync(team.GameId, team.Id, score);
		return true;
    }

    #endregion
}
