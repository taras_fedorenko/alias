﻿namespace AliasApp.Server.Managers;

using AliasApp.Server.Models;

public interface ITeamManager
{
    Task<TeamInfo> AddOrUpdateTeamAsync(TeamInfo team);
    Task<bool> DeleteTeamsAsync(Guid? teamId);
    Task<IEnumerable<TeamInfo>> GetTeamsAsync();
    Task<TeamInfo> GetTeamAsync(Guid teamId);
    Task<bool> SetTeamScoreAsync(Guid teamId, int score);
}
