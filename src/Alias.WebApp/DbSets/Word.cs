namespace AliasApp.Server.Models;

using AliasApp.Server.Enums;
using System.ComponentModel.DataAnnotations;

/// <summary>
/// Represents a word entity.
/// </summary>
public class Word
{
    /// <summary>
    /// Gets or sets the ID of the word.
    /// </summary>
    [Key]
    public int Id { get; set; }

    /// <summary>
    /// Gets or sets the level of the word.
    /// </summary>
    [Required]
    public Level Level { get; set; }

    /// <summary>
    /// Gets or sets the value of the word.
    /// </summary>
    [Required]
    public string? Value { get; set; }
}
