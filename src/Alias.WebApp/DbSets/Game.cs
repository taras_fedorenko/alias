namespace AliasApp.Server.Models;

using System.ComponentModel.DataAnnotations;

/// <summary>
/// Represents a game entity.
/// </summary>
public class Game
{
    /// <summary>
    /// Gets or sets the unique identifier of the game.
    /// </summary>
    [Key]
    public Guid Id { get; set; }

    /// <summary>
    /// Gets or sets the list of teams participating in the game.
    /// </summary>
    public List<Team> Teams { get; set; } = [];

    /// <summary>
    /// Gets or sets the index of the current team.
    /// </summary>
    public int CurrentTeamIndex { get; set; }

    /// <summary>
    /// Gets or sets the index of the current player.
    /// </summary>
    public int CurrentPlayerIndex { get; set; }
}
