namespace AliasApp.Server.Models;

using System.ComponentModel.DataAnnotations;

public class Team
{
    /// <summary>
    /// Gets or sets the ID of the team.
    /// </summary>
    [Key]
    public Guid Id { get; set; }

    /// <summary>
    /// Gets or sets the score of the team.
    /// </summary>
    public int? Score { get; set; }

	/// <summary>
	/// Gets or sets the list of players in the team.
	/// </summary>
	public List<Player> Players { get; set; } = [];

    /// <summary>
    /// Gets or sets the name of the team.
    /// </summary>
    [Required]
    public required string Name { get; set; }

    /// <summary>
    /// Gets or sets the ID of the game associated with the team.
    /// </summary>
    [Required]
    public Guid GameId { get; set; }

	/// <summary>
	/// Gets or sets the game associated with the team.
	/// </summary>
	public Game? Game { get; set; }
}

public class Player
{
    /// <summary>
    /// Gets or sets the ID of the player.
    /// </summary>
    [Key]
    public Guid Id { get; set; }

    /// <summary>
    /// Gets or sets the name of the player.
    /// </summary>
    [Required]
    public required string Name { get; set; }

    /// <summary>
    /// Gets or sets the team the player belongs to.
    /// </summary>
    public Team? Team { get; set; }

    /// <summary>
    /// Gets or sets the ID of the team the player belongs to.
    /// </summary>
    [Required]
    public Guid TeamId { get; set; }
}
