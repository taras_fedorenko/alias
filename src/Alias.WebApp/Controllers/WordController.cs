namespace AliasApp.Server.Controllers;

using AliasApp.Server.Models;
using AliasApp.Server.Managers;
using Microsoft.AspNetCore.Mvc;
using AliasApp.Server.Enums;

[ApiController]
[Route("api/[controller]")]
public class WordController : ControllerBase
{

    private readonly ILogger<WordController> _logger;
    private readonly IWordManager _wordManager;

    public WordController(ILogger<WordController> logger, IWordManager wordManager) {
	    _logger = logger;
	    _wordManager = wordManager;
    }
    
    [HttpGet("getAllWords/{level}")]
    public async Task<IEnumerable<WordInfo>> GetWordsAsync(Level level)
    {
        return await _wordManager.GetWordsAsync(level);
    }

    [HttpGet("getWordByValue/{value}")]
    public async Task<WordInfo> GetWordAsync(string value)
    {
        return await _wordManager.GetWordAsync(value);
    }

    [HttpDelete("deleteWord/{wordId}")]
    public async Task<bool> DeleteWordsAsync(int wordId)
    {
        return await _wordManager.DeleteWordAsync(wordId);
    }

    [HttpPost("addWord/{level}/{value}")]
    public async Task<bool> AddWordAsync(Level level, string value)
    {
        return await _wordManager.AddWordAsync(level, value);
    }

    [HttpGet("getRandomWord/{gameId}/{level}")]
    public async Task<WordInfo> GetRandomWordAsync(Level level, Guid gameId)
    {
        return await _wordManager.GetRandomWordAsync(level, gameId);
    }
}
