﻿namespace AliasApp.Server.Controllers;

using AliasApp.Server.Models;
using AliasApp.Server.Managers;
using Microsoft.AspNetCore.Mvc;

[ApiController]
[Route("api/[controller]")]
public class GameController : ControllerBase
{

	private readonly ILogger<GameController> _logger;
	private readonly IGameManager _gameManager;

	public GameController(ILogger<GameController> logger, IGameManager gameManager) {
		_logger = logger;
		_gameManager = gameManager;
	}

    [HttpGet("getAllGames")]
    public async Task<IEnumerable<GameInfo>> GetGamesAsync()
    {
        return await _gameManager.GetGamesAsync();
    }

    [HttpGet("getGame/{gameId}")]
    public async Task<GameInfo> GetGameAsync(Guid gameId)
    {
        return await _gameManager.GetGameAsync(gameId);
    }

    [HttpDelete("deleteGame/{gameId}")]
    public async Task<bool> DeleteGameAsync(Guid gameId)
    {
        return await _gameManager.DeleteGameAsync(gameId);
    }

    [HttpPost("addGame")]
    public async Task<GameInfo> AddGameAsync()
    {
        return await _gameManager.AddGameAsync();
    }

	[HttpPost("startGame/{gameId}")]
	public async Task<IActionResult> StartGameAsync(Guid gameId)
	{
		await _gameManager.NotifyStartGameAsync(gameId);
		return Ok();
	}

	[HttpPost("updateGameIndexes")]
    public async Task<GameInfo> UpdateGameIndexesAsync([FromBody] GameInfo game)
    {
        return await _gameManager.UpdateGameIndexesAsync(game);
    }
}
