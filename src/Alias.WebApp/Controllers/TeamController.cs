namespace AliasApp.Server.Controllers;

using AliasApp.Server.Models;
using AliasApp.Server.Managers;
using Microsoft.AspNetCore.Mvc;

[ApiController]
[Route("api/[controller]")]
public class TeamController : ControllerBase
{

	private readonly ILogger<TeamController> _logger;
	private readonly ITeamManager _teamManager;

	public TeamController(ILogger<TeamController> logger, ITeamManager teamManager) {
		_logger = logger;
		_teamManager = teamManager;
	}
	
    [HttpGet("getAllTeams")]
    public async Task<IEnumerable<TeamInfo>> GetTeamsAsync()
    {
        return await _teamManager.GetTeamsAsync();
    }

    [HttpGet("getTeam/{teamId}")]
    public async Task<TeamInfo> GetTeamAsync(Guid teamId)
    {
        return await _teamManager.GetTeamAsync(teamId);
    }

    [HttpDelete("deleteTeam/{teamId?}")]
    public async Task<bool> DeleteTeamsAsync(Guid? teamId)
    {
        return await _teamManager.DeleteTeamsAsync(teamId);
    }

    [HttpPost("addTeam")]
    public async Task<TeamInfo> AddTeamAsync([FromBody] TeamInfo team)
    {
        return await _teamManager.AddOrUpdateTeamAsync(team);
    }

    [HttpPost("setScore/{teamId}/{score}")]
    public async Task<bool> SetTeamScoreAsync(Guid teamId, int score)
    {
        return await _teamManager.SetTeamScoreAsync(teamId, score);
    }
}
