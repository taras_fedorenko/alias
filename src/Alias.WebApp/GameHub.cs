﻿namespace AliasApp.Server;

using Microsoft.AspNetCore.SignalR;

public class GameHub : Hub
{
	public async Task SendMessage(string message)
	{
		await Clients.All.SendAsync("ReceiveMessage", message);
	}

	public async Task StartGame(string gameId)
	{
		await Clients.Group(gameId).SendAsync("StartGame");
	}

	public async Task JoinGameGroup(string gameId)
	{
		await Groups.AddToGroupAsync(Context.ConnectionId, gameId);
	}

	public async Task LeaveGameGroup(string gameId)
	{
		await Groups.RemoveFromGroupAsync(Context.ConnectionId, gameId);
	}
}
