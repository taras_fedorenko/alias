#docker compose local plan

docker-compose up -d --build

docker-compose down

docker login

docker tag alias-frontend:latest brainman67/alias-frontend:latest
docker push brainman67/alias-frontend:latest

docker tag alias-backend:latest brainman67/alias-backend:latest
docker push brainman67/alias-backend:latest

#DigitalOcean Droplet with docker settings plan

ssh -l root IpAdress

docker login

docker pull brainman67/alias-backend:latest
docker pull brainman67/alias-frontend:latest
docker pull postgres:latest

docker network create alias

#docker network ls

#In all connection strings to services in other containers you must write conteiner name as server name
#In alias-backend connection string you need set alias-postgres like a server name for correct connection to db

docker run -d --network=alias --name alias-postgres -p 5432:5432 -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=postgres -e POSTGRES_DB=Alias postgres:latest
docker run -d --network=alias --name alias-backend -p 5006:80 brainman67/alias-backend:latest
docker run -d --network=alias --name alias-frontend -p 8088:81 brainman67/alias-frontend:latest

#docker exec -it alias-backend ls

#docker exec -it alias-backend apk update
#docker exec -it alias-backend apk add nano
#docker exec -it alias-backend nano appsettings.json

#docker restart alias-backend

#docker stop $(docker ps -q)

#docker rm $(docker ps -a -q)
